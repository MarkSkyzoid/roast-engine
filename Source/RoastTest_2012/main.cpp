#include "roastengine/RoastEngine.h"

#include "RoastTypes.h"
#include "TestRunner.h"
#include "FrameAllocatorTest.h"
#include "MemoryTest.h"
#include "ConsoleOutputPolicy.h"
#include "conio.h"

using namespace roast_test;

INT WINAPI wWinMain(HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPWSTR    lpCmdLine,
					int       nCmdShow)
{

	//roast::windowsHelper::ShowConsole(); //Explicit call to allow console based tests.
	
	//This should be moved one day from here, but let's write it here for now.
	/*typedef ConsoleOutputPolicy DefaultOutputPolicy;

	TestRunner<FrameAllocatorTest,DefaultOutputPolicy>::Run();
	TestRunner<MemoryGeneralTest,DefaultOutputPolicy>::Run();

	std::cout << "Please, press a button to continue..." << std::endl;
	getch();*/

	return RoastMain(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}
#ifndef _H_MEMORY_TEST__
#define _H_MEMORY_TEST__

#include <string>
#include "TestUtils.h"
#include "memory\Memory.h"

namespace roast_test
{
	class MemoryGeneralTest
	{
	public:
		struct TestStruct
		{
			char a;
			char b;
			int c;
		};

		struct TestStruct16
		{
			int a;
			int b;
			int c;
			int d;
		};

		typedef roast::MemoryArena<roast::LinearAllocator, roast::SingleThreadPolicy, roast::NoBoundsChecking,roast::SimpleMemoryTracking,roast::NoMemoryTagging> basic_arena;
		typedef roast::MemoryArena<roast::PoolAllocator<8, 16, 10>, roast::SingleThreadPolicy, roast::NoBoundsChecking,roast::ExtendedMemoryTracking,roast::NoMemoryTagging> pool_arena;

		void TestAlignmentAlloc(basic_arena& arena, int alignment)
		{
			TestStruct* test_struct = RE_NEW_ALIGNED(TestStruct, arena, alignment);
			TestAssert( ((uintptr_t)test_struct & (alignment - 1)) == 0); 
			RE_DELETE(test_struct, arena);
		}

		bool Run()
		{
			const int memoryArenaSize = 4096;
			roast::HeapArea area(memoryArenaSize);

			basic_arena test_arena(area);
			

			roast::HeapArea area51(72);
			pool_arena lello_arena(area51);
			TestStruct* t_struct1 = nullptr; 

			//We verify that the default memory alignment is 16 bytes.
			//TestAssert( ((uintptr_t)t_struct1 & (16 - 1)) == 0); 
			
			TestStruct* kept_ptr = nullptr;
			for(int i = 0; i < 10; ++i)
			{
				kept_ptr = t_struct1;
				t_struct1 = RE_NEW(TestStruct, lello_arena);
				if(!t_struct1) break;
				t_struct1->a = 'm';
				t_struct1->b = 'e';
				t_struct1->c = i;
			}

			std::cout << "ketp_ptr = " << "{ " << kept_ptr->a << ", " << kept_ptr->b << ", " << kept_ptr->c << "}" << std::endl;

			RE_DELETE(kept_ptr, lello_arena);

			kept_ptr = RE_NEW(TestStruct, lello_arena);

			kept_ptr->a = 'n'; kept_ptr->b = 'e'; kept_ptr->c = 101;

			std::cout << "ketp_ptr = " << "{ " << kept_ptr->a << ", " << kept_ptr->b << ", " << kept_ptr->c << "}" << std::endl;

			RE_DELETE(kept_ptr, lello_arena);

			size_t t = sizeof(TestStruct16);
			TestStruct16* test_struct_16 = RE_NEW(TestStruct16, lello_arena);

			test_struct_16->a = 10; test_struct_16->b = 11; test_struct_16->c = 12; test_struct_16->d = 13;

			lello_arena.GetAllocator()->Reset();

			

			TestAlignmentAlloc(test_arena,4);
			TestAlignmentAlloc(test_arena,8);
			TestAlignmentAlloc(test_arena,16);
			TestAlignmentAlloc(test_arena,32);

			TestStruct* t_struct2 = RE_NEW_ALIGNED(TestStruct, test_arena, 8);

			t_struct2->a = 't';
			t_struct2->b = 'e';
			t_struct2->c = 94;
			
			RE_DELETE(t_struct1, test_arena);
			RE_DELETE(t_struct2, test_arena);

			return true;
		}

		static std::string GetTestName() { return "Memory General 1 - Alignment & Linear Allocator"; }
	};

}

#endif

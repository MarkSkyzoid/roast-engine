#ifndef TestRunner_h__
#define TestRunner_h__
#include <stdlib.h>
#include <iosfwd>

//=======================================================================================================
// Author: Stefano Musumeci																			  	
// Date: 11/08/2013																				  
// Utility template class that uses implicit typing to avoid inheritance on unit test classes 
//=======================================================================================================
namespace roast_test
{
	/* Implicit requirements for T:
	*  Must have a parameterless constructor
	*  Must have a method called Run that returns a bool.
	*  Must have a static method called GetTestName that returns a std::string.
	*/
	template <class T, class OutputPolicy>
	class TestRunner
	{
		public:
			static bool Run()
			{
				// Could add try/catch block and exception / error safety or init fixture etc, this is only a starting point.
				bool result = false;
				OutputPolicy::Print( "Starting test" + T::GetTestName() );
				try
				{
					T testInstance;
					result = testInstance.Run();
				}
				catch(...) { }
				OutputPolicy::Print(T::GetTestName() + " Test result is " + (result == true ? "ok" : "failed"));

				roast::math::Frustum frust;
				frust.Init(RE_PI_OVER_4, 800.0f / 600.0f, 1, 1000 );
				frust.SetPoint(roast::math::Vec3(0,0,0),roast::math::Vec3::GForwardVec, roast::math::Vec3::GUpVec);
				return result;
			}
	};
}

#endif
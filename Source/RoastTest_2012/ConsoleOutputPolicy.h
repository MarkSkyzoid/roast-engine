#ifndef ConsoleOutputPolicy_h__
#define ConsoleOutputPolicy_h__

#include <string>
#include <iostream>

//=======================================================================================================
// Author: Stefano Musumeci																			  	
// Date: 11/08/2013																				  
// Policy class for the TestRunner that prints output in the standard console 
//=======================================================================================================

namespace roast_test
{
	class ConsoleOutputPolicy
	{
	public:
		static void Print(const std::string& text)
		{
			std::cout << text << std::endl;
		}
	};
}

#endif
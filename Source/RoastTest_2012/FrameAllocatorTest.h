#include "memory\FrameAllocator.h"
#include <string>
#include "TestUtils.h"

namespace roast_test
{
	class FrameAllocatorTest
	{
	public:

		bool Run()
		{
			const int testAllocationSize = 1024 * 6;
			const int testAlignmentSize = 16;
			roast::FrameAllocator allocator;
			TestAssert( allocator.Init(testAllocationSize,testAlignmentSize) );
			roast::MemoryFrame frame = allocator.GetFrame(roast::eFrameHeapType::HEAP_LOWER);
			TestAssert( allocator.Alloc(testAllocationSize / 2, roast::eFrameHeapType::HEAP_LOWER) != NULL );
			TestAssert( allocator.Alloc(testAllocationSize / 2, roast::eFrameHeapType::HEAP_LOWER) != NULL );
			TestAssert( allocator.Alloc(testAllocationSize, roast::eFrameHeapType::HEAP_LOWER) == NULL );
			allocator.Dealloc(frame);
			TestAssert( allocator.Alloc(testAllocationSize, roast::eFrameHeapType::HEAP_LOWER) != NULL );
			return true;
		}

		static std::string GetTestName() { return "Frame Allocator"; }
	};

}
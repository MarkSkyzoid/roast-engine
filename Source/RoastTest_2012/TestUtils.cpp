#include "TestUtils.h"
#include <exception>

namespace roast_test
{
	void TestAssert(bool expression)
	{
		if( expression == false )
		{
			throw std::exception();
		}
	}
}
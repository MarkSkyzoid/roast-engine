#ifndef TestUtils_h__
#define TestUtils_h__

//=======================================================================================================
// Author: Stefano Musumeci																			  	
// Date: 11/08/2013																				  
// Header file for utiliy functions for tests 
//=======================================================================================================

namespace roast_test
{
	void TestAssert(bool expression);
}

#endif
#ifndef Utils_h__
#define Utils_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 08/09/2013																				  
// Utilities headers collection.
//=============================================================================================================

#include "PointerUtils.h"
#include "DebugUtils.h"

//Hash functions
#include "HashFNV.h"

#endif // Utils_h__

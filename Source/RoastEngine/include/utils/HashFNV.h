#ifndef HashFNV_h__
#define HashFNV_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 22/01/2014																				  
// Fowler-Noll-Vo hash (FNV-1a, 32 bit)function. Pretty fast. Also added support for compile time hashing which is quite cool :P
//=============================================================================================================

unsigned int HashFNV(const char* s);

#define HASH_FNV_INTERNAL(s) 2166136261u ^ \
							(16777619u * static_cast<unsigned int>(s[0])) ^ \
							(16777619u * static_cast<unsigned int>(s[1])) ^ \
							(16777619u * static_cast<unsigned int>(s[2])) ^ \
							(16777619u * static_cast<unsigned int>(s[3])) ^ \
							(16777619u * static_cast<unsigned int>(s[4])) ^ \
							(16777619u * static_cast<unsigned int>(s[5])) ^ \
							(16777619u * static_cast<unsigned int>(s[6])) ^ \
							(16777619u * static_cast<unsigned int>(s[7])) ^ \
							(16777619u * static_cast<unsigned int>(s[8])) ^ \
							(16777619u * static_cast<unsigned int>(s[9])) ^ \
							(16777619u * static_cast<unsigned int>(s[10])) ^ \
							(16777619u * static_cast<unsigned int>(s[11])) ^ \
							(16777619u * static_cast<unsigned int>(s[12])) ^ \
							(16777619u * static_cast<unsigned int>(s[13])) ^ \
							(16777619u * static_cast<unsigned int>(s[14])) ^ \
							(16777619u * static_cast<unsigned int>(s[15])) ^ \
							(16777619u * static_cast<unsigned int>(s[16])) ^ \
							(16777619u * static_cast<unsigned int>(s[17])) ^ \
							(16777619u * static_cast<unsigned int>(s[18])) ^ \
							(16777619u * static_cast<unsigned int>(s[19])) ^ \
							(16777619u * static_cast<unsigned int>(s[20])) ^ \
							(16777619u * static_cast<unsigned int>(s[21])) ^ \
							(16777619u * static_cast<unsigned int>(s[22])) ^ \
							(16777619u * static_cast<unsigned int>(s[23])) ^ \
							(16777619u * static_cast<unsigned int>(s[24])) ^ \
							(16777619u * static_cast<unsigned int>(s[25])) ^ \
							(16777619u * static_cast<unsigned int>(s[26])) ^ \
							(16777619u * static_cast<unsigned int>(s[27])) ^ \
							(16777619u * static_cast<unsigned int>(s[28])) ^ \
							(16777619u * static_cast<unsigned int>(s[29])) ^ \
							(16777619u * static_cast<unsigned int>(s[30])) ^ \
							(16777619u * static_cast<unsigned int>(s[31])) ^ \
							(16777619u * static_cast<unsigned int>(s[32])) ^ \
							(16777619u * static_cast<unsigned int>(s[33])) ^ \
							(16777619u * static_cast<unsigned int>(s[34])) ^ \
							(16777619u * static_cast<unsigned int>(s[35])) ^ \
							(16777619u * static_cast<unsigned int>(s[36])) ^ \
							(16777619u * static_cast<unsigned int>(s[37])) ^ \
							(16777619u * static_cast<unsigned int>(s[38])) ^ \
							(16777619u * static_cast<unsigned int>(s[39])) ^ \
							(16777619u * static_cast<unsigned int>(s[40])) ^ \
							(16777619u * static_cast<unsigned int>(s[41])) ^ \
							(16777619u * static_cast<unsigned int>(s[42])) ^ \
							(16777619u * static_cast<unsigned int>(s[43])) ^ \
							(16777619u * static_cast<unsigned int>(s[44])) ^ \
							(16777619u * static_cast<unsigned int>(s[45])) ^ \
							(16777619u * static_cast<unsigned int>(s[46])) ^ \
							(16777619u * static_cast<unsigned int>(s[47])) ^ \
							(16777619u * static_cast<unsigned int>(s[48])) ^ \
							(16777619u * static_cast<unsigned int>(s[49])) ^ \
							(16777619u * static_cast<unsigned int>(s[50])) ^ \
							(16777619u * static_cast<unsigned int>(s[51])) ^ \
							(16777619u * static_cast<unsigned int>(s[52])) ^ \
							(16777619u * static_cast<unsigned int>(s[53])) ^ \
							(16777619u * static_cast<unsigned int>(s[54])) ^ \
							(16777619u * static_cast<unsigned int>(s[55])) ^ \
							(16777619u * static_cast<unsigned int>(s[56])) ^ \
							(16777619u * static_cast<unsigned int>(s[57])) ^ \
							(16777619u * static_cast<unsigned int>(s[58])) ^ \
							(16777619u * static_cast<unsigned int>(s[59])) ^ \
							(16777619u * static_cast<unsigned int>(s[60])) ^ \
							(16777619u * static_cast<unsigned int>(s[61])) ^ \
							(16777619u * static_cast<unsigned int>(s[62])) ^ \
							(16777619u * static_cast<unsigned int>(s[63]))

//Compile time, bitches!
#define HASH_FNV(s) (HASH_FNV_INTERNAL(  s "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0" \
   											"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0" \
											"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0" \
											"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0") ) //0s are for padding and making sure the string terminates.

#endif // HashFNV_h__

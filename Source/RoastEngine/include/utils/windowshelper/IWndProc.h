#ifndef IWndProc_h__
#define IWndProc_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 13/01/2014																			  
// Interface for the windows procedure. It will most likely be inherited by application classes to define a custom window procedure.
//=======================================================================================================

namespace roast
{
	class IWndProc
	{
	public:
		virtual LRESULT WindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) = 0;
		virtual void BeforeRegisterWindowClass(WNDCLASSEX &wc) = 0;
	};
}

#endif // IWndProc_h__

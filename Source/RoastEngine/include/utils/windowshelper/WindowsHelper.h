#ifndef WindowsHelper_h__
#define WindowsHelper_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 10/08/2013																				  
// Helper functions like creating a window, setting callbacks and so on.
//=======================================================================================================

#define WIN32_LEAN_AND_MEAN             // Excludes useless stuff from windows headers.

#include <windows.h>
#include <windowsx.h>

#include <D3Dcommon.h>
#include <DXGI.h>

#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")
namespace roast
{
	namespace windowsHelper
	{
		struct REDeviceSettings
		{
			UINT AdapterOrdinal;
			D3D_DRIVER_TYPE DriverType;
			UINT Output;
			DXGI_SWAP_CHAIN_DESC sd;
			UINT32 CreateFlags;
			UINT32 SyncInterval;
			DWORD PresentFlags;
			bool AutoCreateDepthStencil; // DXUT will create the depth stencil resource and view if true
			DXGI_FORMAT AutoDepthStencilFormat;
			D3D_FEATURE_LEVEL DeviceFeatureLevel;
		};

		HRESULT WINAPI RECreateWindow( UINT default_width = 640,  UINT default_height = 480, const WCHAR* window_title = L"Roast Engine", HINSTANCE hInstance = NULL, HICON hIcon = NULL, bool show_console = false );
		HRESULT WINAPI RECreateDevice( bool windowed, UINT screen_width, UINT screen_height ); //Creates a DirectX 11 Device.
		HRESULT WINAPI RECreateWindowAndDevice( bool windowed = true, UINT screen_width = 640, UINT screen_height = 480, const WCHAR* window_title = L"Roast Engine", HINSTANCE hInstance = NULL, HICON hIcon = NULL, bool show_console = false );

		LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam);
		void REWindowsMainLoop();

		void REApplyDefaultDeviceSettings( REDeviceSettings* settings );

		HWND GetHWnd();

		void ShowConsole(); //Shows a command prompt console.
		void HideConsole(); //Hides it.
	}
}

#endif // WindowsHelper_h__

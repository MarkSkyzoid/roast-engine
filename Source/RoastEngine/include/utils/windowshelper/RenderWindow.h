#ifndef RenderWindow_h__
#define RenderWindow_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 13/01/2014																			  
// Class to represent a window with a rendering context.
//=======================================================================================================

#include <d3d11_1.h>
#include <D3Dcompiler.h>
#include <string>

#include "IWndProc.h"

namespace roast
{
	class RenderWindow
	{
	protected:
		void UpdateState();

		HWND m_hwnd;

		int m_width;
		int m_height;
		int m_left;
		int m_top;

		std::wstring m_caption;
		DWORD m_style;

		int m_swapChain;

	public:
		RenderWindow();
		~RenderWindow();

		void Init( IWndProc* wind_proc_obj );
		void Shutdown();
		void Draw();

		bool Update(); //Message pump.

		void Resize( int width, int height );

		//Getters and Setters.
		void SetWidth( int width );
		void SetHeight( int height );
		void SetSize( int width, int height );
		void SetPosition( int left, int top );

		void SetCaption( const std::wstring& caption );
		void SetStyle( DWORD style );

		void SetSwapChain( int swapchain ) { m_swapChain = swapchain; }

		int GetWidth() const;
		int GetHeight() const;
		int GetLeft() const;
		int GetTop() const;

		std::wstring GetCaption() const { return m_caption; }
		DWORD GetStyle() const { return m_style; }

		int GetSwapChain() const { return m_swapChain; }

		HWND GetHandle() const { return m_hwnd; }


	};
}

#endif // RenderWindow_h__

#ifndef SmartPointer_h__
#define SmartPointer_h__

#include <map>

template <class T>
class SmartPointer
{
private:
	
	typedef std::map<T*, unsigned int> mapType;
	//typedef mapType::iterator mapType_it;

	//static mapType m_references;

	static mapType& GetRefCountMap()
	{
		static mapType ref_count_map;

		return ref_count_map;
	}

	T* m_ref;

public:
	SmartPointer() : m_ref(nullptr) {}
	SmartPointer(T* p)
	{
		//Do stuff
	}

	~SmartPointer()
	{
		Release();
	}

	void Release()
	{
		typename mapType::iterator it = GetRefCountMap().find(GetRef());
		if(it != GetRefCountMap().end())
		{
			--it->second;

			if(it->second <= 0)
			{
				delete it->first; // delete pointer.
				GetRefCountMap().erase(it); // delete the entry.
			}
		}

		m_ref = nullptr;
	}

	T* GetRef() { return m_ref; }
	const T* GetRef() const { return m_ref; }

	unsigned int GetRefCount() { return GetRefCountMap()[m_ref]; }

	T& operator ->()
	{
		return *this;
	}

	template<typename T> SmartPointer& operator= (T* rhs)
	{
		//Increase the references if it exists, else create a new entry.
		mapType::iterator it = GetRefCountMap().find(rhs);
		if(it != GetRefCountMap().end())
		{
			++it->second;
		}
		else
		{
			GetRefCountMap()[rhs] = 1; //1 reference now.
		}

		m_ref = rhs;

		return *this;
	}

	template<typename T> SmartPointer& operator= (const SmartPointer<T>& rhs)
	{
		//Increase the references if it exists, else create a new entry.
		typename mapType::iterator it = GetRefCountMap().find(rhs->GetRef());
		if( it != GetRefCountMap().end() )
		{
			++it->second;
		}
		else
		{
			GetRefCountMap()[rhs->GetRef()] = 1; //1 reference now.
		}

		m_ref = rhs->GetRef();

		return *this;
	}
};

#endif // SmartPointer_h__

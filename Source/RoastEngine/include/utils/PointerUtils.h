#ifndef PointerUtils_h__
#define PointerUtils_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 30/08/2013																				  
// Pointers related utilities.
//=============================================================================================================

#include "RoastTypes.h"

namespace roast
{
	namespace pointerUtils
	{
		//Aligns a pointer to the next aligned address.
		void* AlignUp(void* const ptr, size_t alignment);
	}
}

#endif // PointerUtils_h__

#ifndef DebugUtils_h__
#define DebugUtils_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 08/09/2013																				  
// Debugging related utilities.
//=============================================================================================================

#define RE_SOURCEINFO roast::debug::SourceInfo( __FILE__, __LINE__ )

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32)
#define RE_BREAKPOINT __debugbreak()
#else
#define RE_BREAKPOINT //TODO
#endif

#include <cassert>
#define RE_ASSERT(condition) assert(condition)

namespace roast
{
	namespace debug
	{
		//Structure the holds filename and line number. Useful for debugging purposes.
		struct SourceInfo
		{
			const char* file;
			int line;

			SourceInfo( const char* file_name, int line_number ) : file(file_name), line(line_number){}
		};
	}
}

#endif // DebugUtils_h__

#ifndef Matrix_h__
#define Matrix_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 22/10/2013																				  
// Representation of a 4x4 matrix, with operations and stuff.
// Thank you, again, Tim Armstrong, for the awesome soundtrack you provided while I was coding.
//=======================================================================================================

#include "Vector.h"
#include <xnamath.h>
namespace roast
{
	namespace math
	{
		class Mat4x4 : public D3DXMATRIX
		{
		public:

			//Modifiers
			inline void SetPosition(const Vec3& pos)
			{
				m[3][0] = pos.x;
				m[3][1] = pos.y;
				m[3][2] = pos.z;
				m[3][3] = 1.0f;
			}

			inline void SetPosition(const Vec4& pos)
			{
				m[3][0] = pos.x;
				m[3][1] = pos.y;
				m[3][2] = pos.z;
				m[3][3] = pos.w;
			}

			//Accessors/Computing methods
			inline Vec3 GetPosition() const
			{ 
				return Vec3(m[3][0], m[3][1], m[3][2]); 
			}

			inline Vec4 Transform(Vec4& v) const
			{
				Vec4 temp;
				D3DXVec4Transform(&temp, &v, this);
				return temp;
			}

			inline Vec3 Transform(Vec3& v) const
			{
				Vec4 temp(v), out;
				D3DXVec4Transform(&out, &temp, this);
				return out;
			}

			inline Mat4x4 Inverse() const
			{
				Mat4x4 out;
				D3DXMatrixInverse(&out, NULL, this);
				return out;
			}

			//Initialization methods.
			inline void BuildTranslation(const Vec3& pos)
			{
				*this = Mat4x4::Identity;
				m[3][0] = pos.x; m[3][1] = pos.y; m[3][2] = pos.z;
			}

			inline void BuildTranslation(const float x, const float y, const float z)
			{
				*this = Mat4x4::Identity;
				m[3][0] = x; m[3][1] = y; m[3][2] = z;
			}

			inline void BuildRotationX(const float radians) { D3DXMatrixRotationX(this, radians); }
			inline void BuildRotationY(const float radians) { D3DXMatrixRotationY(this, radians); }
			inline void BuildRotationZ(const float radians) { D3DXMatrixRotationZ(this, radians); }

			inline void BuildRotationYawPitchRoll(const float yaw_radians, const float pitch_radians, const float roll_radians) 
			{ D3DXMatrixRotationYawPitchRoll(this, yaw_radians, pitch_radians, roll_radians); }

			inline void BuildRotationQuat(const class Quaternion& q);
			//TODO: Implement { D3DXMatrixRotationQuaternion( this, &q ); }

			inline void BuildRotationLookAt(const Vec3& eye, const Vec3& at, const Vec3& up)
			{ D3DXMatrixLookAtLH(this, &eye, &at, &up); }

			//Copy constructor
			Mat4x4(const D3DXMATRIX& mat) { memcpy(&m, &mat.m, sizeof(mat.m)); }
			Mat4x4() : D3DXMATRIX() { }

			static const Mat4x4 Identity;
		};

		//Matrix operators
		inline Mat4x4 operator * (const Mat4x4& a, const Mat4x4& b)
		{
			Mat4x4 out;
			D3DXMatrixMultiply( &out, &a, &b );
			return out;
		}
	}
}

#endif // Matrix_h__

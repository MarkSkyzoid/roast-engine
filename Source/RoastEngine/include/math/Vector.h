#ifndef Vector_h_
#define Vector_h_

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 22/10/2013																				  
// Representation of vectors (vec3 and vec4), with operations and stuff.
// Thank you, Tim Armstrong, for the awesome soundtrack you provided while I was coding.
//=======================================================================================================
#include <D3DX10math.h>
#include <string>

namespace roast
{
	namespace math
	{
		class Vec3 : public D3DXVECTOR3
		{
		public:
			//Operations
			inline float Lenght() { return D3DXVec3Length(this); }
			inline float LenghtSquared() { return Dot(*this); } //Faster implementation. Dot with self. Taken from Tricks of the Pinga Roast gurus =)
			
			inline Vec3* Normalize() { return static_cast<Vec3*>( D3DXVec3Normalize(this, this) ); }

			inline float Dot(const Vec3& b) const { return D3DXVec3Dot(this, &b); }

			inline Vec3 Cross(const Vec3& b) const
			{
				Vec3 out;
				D3DXVec3Cross(&out, this, &b);
				return out;
			}

			inline std::string ToString()
			{
				char buf[512];
				sprintf(buf,"%f : %f : %f", x, y, z);
				std::string str = buf;
				return str;
			}

			//Copy constructor.
			Vec3(const D3DXVECTOR3& v3)
			{ x = v3.x; y = v3.y; z = v3.z; }
			
			Vec3() : D3DXVECTOR3() { }
			
			Vec3(const float _x, const float _y, const float _z)
			{ x = _x; y = _y; z = _z; }
			
			inline Vec3(const class Vec4& v4);

			//Globals
			static const Vec3 GUpVec;
			static const Vec3 GRightVec;
			static const Vec3 GForwardVec;
		};

		class Vec4 : public D3DXVECTOR4
		{
		public:
			//Operations
			inline float Lenght() { return D3DXVec4Length(this); }

			inline Vec4* Normalize() { return static_cast<Vec4*>( D3DXVec4Normalize(this, this) ); }

			inline float Dot(const Vec4& b) { return D3DXVec4Dot(this, &b); }

			//Cross product is done only on Vec3, so use that!

			inline std::string ToString()
			{
				char buf[512];
				sprintf(buf,"%f : %f : %f : %f", x, y, z, w);
				std::string str = buf;
				return str;
			}

			//Copy constructor
			Vec4(const D3DXVECTOR4& v4)
			{ x = v4.x; y = v4.y; z = v4.z; w = v4.w; }

			Vec4() : D3DXVECTOR4() { }

			Vec4(const float _x, const float _y, const float _z, const float _w)
			{ x = _x; y = _y; z = _z; w = _w; }

			inline Vec4(const class Vec3& v3)
			{ x = v3.x; y = v3.y; z = v3.z; w = 1.0f; }
		};

		inline Vec3::Vec3(const Vec4& v4) { x = v4.x; y = v4.y; z = v4.z; }
	}
}


#endif // Vector_h_

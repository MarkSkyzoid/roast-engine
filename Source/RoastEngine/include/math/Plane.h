#ifndef Plane_h__
#define Plane_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 02/11/2013																				  
// Representation of a plane, with operations and stuff.
//=======================================================================================================

#include <D3DX10math.h>
#include "Vector.h"

namespace roast
{
	namespace math
	{
		class Plane : public D3DXPLANE
		{
		public:
			inline void Normalize()
			{
				float mag_inverse = 1.0f / sqrt(a * a + b * b + c * c);
				a *= mag_inverse;
				b *= mag_inverse;
				c *= mag_inverse;
				d *= mag_inverse;
			}

			//NOTE: If points are sent in counter-clockwise order, the normal will face backwards.
			inline void Init(const Vec3& p0, const Vec3& p1, const Vec3& p2)
			{
				D3DXPlaneFromPoints(this, &p0, &p1, &p2);
				Normalize();
			}

			bool Inside(const Vec3& point) const
			{
				return D3DXPlaneDotCoord(this, &point) >= 0; 
			}

			bool Inside(const Vec3& point, const float radius) const
			{
				float dist = D3DXPlaneDotCoord(this, &point);
				return (dist >= -radius);
			}

			//Accessors
			Vec3 GetNormal() const
			{
				Vec3 n = Vec3(a, b, c);
				n.Normalize();
				return n;
			}
		};
	}
}

#endif // Plane_h__

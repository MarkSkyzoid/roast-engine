#ifndef Quaternion_h__
#define Quaternion_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 02/11/2013																				  
// Representation of a Quaternion, with operations and stuff.
//=======================================================================================================

#include <D3DX10math.h>
#include "Matrix.h"

namespace roast
{
	namespace math
	{
		class Quaternion : public D3DXQUATERNION
		{
		public:
			void Normalize() { D3DXQuaternionNormalize(this, this); }
			
			//Spherical linear interpolation. Coefficient must be in 0 - 1 range.
			void Slerp(const Quaternion& begin, const Quaternion& end, float coef) { D3DXQuaternionSlerp(this, &begin, &end, coef); }

			//Accessors
			void GetAxisAngle(class Vec3& axis, float& angle) const
			{
				D3DXQuaternionToAxisAngle(this, &axis, &angle);
			}

			//Initializers
			void BuildRotYawPitchRoll(const float yaw_radians, const float pitch_radians, const float roll_radians)
			{
				D3DXQuaternionRotationYawPitchRoll(this, yaw_radians, pitch_radians, roll_radians);
			}

			void BuildAxisAngle(const Vec3& axis, const float radians)
			{
				D3DXQuaternionRotationAxis(this, &axis, radians);
			}

			void Build(const class Mat4x4& mat)
			{
				D3DXQuaternionRotationMatrix(this, &mat);
			}

			Quaternion(D3DXQUATERNION& q) : D3DXQUATERNION(q) { }
			Quaternion() : D3DXQUATERNION() { }

			static const Quaternion Identity;
		};

		inline Quaternion operator * (const Quaternion& a, const Quaternion& b)
		{
			Quaternion out;
			D3DXQuaternionMultiply(&out, &a, &b);
			return out;
		}
	}
}

#endif // Quaternion_h__

#ifndef Math_h__
#define Math_h__

#include <cmath>

#include "Vector.h"
#include "Matrix.h"
#include "Quaternion.h"
#include "Frustum.h"

//Useful defines
#define RE_PI			3.14159265358979323846
#define RE_PI_OVER_2	1.57079632679489661923
#define RE_PI_OVER_4	0.785398163397448309616

#define RE_DEG_TO_RAD   RE_PI / 180.0

#endif // Math_h__

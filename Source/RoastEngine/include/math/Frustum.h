#ifndef Frustum_h__
#define Frustum_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 03/11/2013																				  
// View Frustum class. Will be used for frustum culling. 
//=======================================================================================================

#include "math\Plane.h"
#include "math\Vector.h"

namespace roast
{
	namespace math
	{
		enum eFrustumPlane 
		{ 
			eFrustumPlane_Near, 
			eFrustumPlane_Far, 
			eFrustumPlane_Top, 
			eFrustumPlane_Right,
			eFrustumPlane_Bottom, 
			eFrustumPlane_Left,
			eFrustumPlane_NumPlanes 
		};

		enum eFrustumIntersection
		{
			eFrustumIntersection_Inside,
			eFrustumIntersection_Outside,
			eFrustumIntersection_Intersect,
			eFrustumIntersection_LastEnum
		};

		class Frustum
		{
		private:
			roast::math::Plane m_planes[eFrustumPlane_NumPlanes]; //Planes are in camera space.
			roast::math::Vec3  m_nearClip[4]; //Vertices for the near clipping plane.
			roast::math::Vec3  m_farClip[4];  //Same, for far clipping plane.

			float m_fov;    //Field of view.
			float m_aspect; //Aspect ration - w/h.
			float m_near;	//Distance from near clipping plane.
			float m_far;	//Distance from far clipping plane.

			float m_nearWidth;
			float m_nearHeight;
			float m_farWidth;
			float m_farHeight;

		public:
			//Initializers
			Frustum();

			void Init(const float fov, const float aspect, const float near_dist, const float far_dist);

			//Intersection methods
			bool Inside(const roast::math::Vec3& point); 
			bool Inside(const roast::math::Vec3& point, const float radius); //Check if a sphere is inside.
			const roast::math::Plane& GetPlane(eFrustumPlane side) const { return m_planes[side]; }

			//Setters

			//Sets the frustum origin point, along with the looking direction and the up vector.
			void SetPoint(const roast::math::Vec3& point, const roast::math::Vec3& forward, const roast::math::Vec3& up);
			
			void SetFOV(const float fov)
			{
				m_fov = fov;
				Init(m_fov, m_aspect, m_near, m_far);
			}

			void SetAspect(const float aspect)
			{
				m_aspect = aspect;
				Init(m_fov, m_aspect, m_near, m_far);
			}

			void SetNear(const float near_dist)
			{
				m_near= near_dist;
				Init(m_fov, m_aspect, m_near, m_far);
			}

			void SetFar(const float far_dist)
			{
				m_far = far_dist;
				Init(m_fov, m_aspect, m_near, m_far);
			}

			//Rendering
			void Render(); //Renders the frustum for debugging.
		};
	}
}

#endif // Frustum_h__

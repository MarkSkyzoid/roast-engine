#ifndef RoastTypes_h__
#define RoastTypes_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 10/08/2013																				  
// File types for RoastEngine 
//=======================================================================================================


typedef unsigned char u8;
typedef unsigned int uint;

namespace roast
{
	//Non-copyable class (useful) - Inherit from it PRIVATELY.
	class Noncopyable
	{
	private:
		//Simply hide copy constructor and assignment operator.
		Noncopyable( const Noncopyable& );
		Noncopyable& operator= ( const Noncopyable& );

	public:
		Noncopyable() {} //Default constructor.
	};
}

#endif // RoastTypes_h__


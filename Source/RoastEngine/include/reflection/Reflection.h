#ifndef Reflection_h__
#define Reflection_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 22/01/2014																				  
// Main header for reflection.
//=============================================================================================================

#include <algorithm>
#include <stdio.h>
#include <vector>
#include <stack>


//MACROS FOR REFLECTION (assume that Reflector class is named reflector)
#define SCOPE(s) ScopeHelper(scope_helper, HASH_FNV(#s), instance_name, instance_name_hash);

enum eReflectResult
{
	REFLECT_OK = 0,
	REFLECT_ERROR,
	NUM_REFLECT_RESULT
};

namespace roast
{
	namespace reflection
	{
		class Reflectable;

		// Base class for deriving "reflectors"
		class ReflectorBase
		{
		protected:
			std::stack<unsigned int> m_scope;
			unsigned int m_remove;

		public:
			ReflectorBase() : m_remove(0) {}
			virtual ~ReflectorBase() {}

			virtual eReflectResult Reflect( const char* instance_name, 
				unsigned int instance_name_hash,
				void* date,
				size_t size)
			{
				//This functions needs to be overridden.
				return REFLECT_OK;
			}

			// Basic methods for built-in types.
			// Derived classes should override the following functions for built-in types.
			
			virtual eReflectResult Reflect( const char* instance_name, 
											unsigned int instance_name_hash,
											unsigned int& i)
			{
				return Reflect ( instance_name, instance_name_hash, &i, sizeof(unsigned int) );
			}

			virtual eReflectResult Reflect( const char* instance_name, 
											unsigned int instance_name_hash,
											float& f)
			{
				return Reflect ( instance_name, instance_name_hash, &f, sizeof(float) );
			}

			virtual eReflectResult Reflect( const char* instance_name, 
											unsigned int instance_name_hash,
											bool& b)
			{
				return Reflect ( instance_name, instance_name_hash, &b, sizeof(bool) );
			}

			virtual eReflectResult Reflect( const char* instance_name, 
											unsigned int instance_name_hash,
											int& e,
											const char* enum_string)
			{
				return Reflect ( instance_name, instance_name_hash, &e, sizeof(int) );
			}

			virtual eReflectResult Reflect( const char* instance_name, 
											unsigned int instance_name_hash,
											Reflectable* & p,
											Reflectable*(*new_helper)())
			{
			}

			virtual eReflectResult Reflect( const char* name, void* instance, void(*method)(void*) ) 
			{
			}

			virtual void Rename( unsigned int old_name_hash )
			{
			}

			virtual void PushScope( const char* instance_name, unsigned int class_name_hash )
			{
				m_scope.push(class_name_hash);
			}

			virtual void PopScope()
			{
				m_scope.pop();
			}

			unsigned int GetScopeNameHash()
			{
				m_scope.size() > 0 ? m_scope.top() : 0;
			}

			size_t GetStackDepth()  { return m_scope.size(); }

			void PushRemove()		{ ++m_remove;			 }
			void PopRemove()		{ --m_remove;			 }
			bool GetRemove()        { return m_remove > 0;   }
		};


		class ScopeHelper
		{
		private:
			ReflectorBase* m_reflector;
		
		public:
			ScopeHelper( ReflectorBase* reflector,
						 unsigned int class_name_hash,
						 const char* instance_name,
						 unsigned int instance_name_hash) : m_reflector(reflector)
			{
				m_reflector->PushScope(instance_name, class_name_hash);
			}

			~ScopeHelper()
			{
				m_reflector->PopScope();
			}
		};

		class Reflectable
		{
		public:
			virtual ~Reflectable(){}

			virtual void Reflect( ReflectorBase* reflector,
				const char* instance_name,
				unsigned int instance_name_hash ) = 0;
		};

	}
}

#endif // Reflection_h__

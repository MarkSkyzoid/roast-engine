#ifndef RoastEngineStd_h__
#define RoastEngineStd_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 11/09/2013																				  
// Standard stuff (mostly windows related) like includes and typedefs, used by the engine
//=============================================================================================================

//Include this first as other dependencies might cause problems
//#include "utils/windowshelper/WindowsHelper.h"
#include "utils/windowshelper/RenderWindow.h"

//Utilities includes
#include "utils/Utils.h"

//Math includes
#include "math/Math.h"

//Memory includes
#include "memory/Memory.h"

#include <crtdbg.h>

extern INT WINAPI RoastMain(HINSTANCE hInstance,
							HINSTANCE hPrevInstance,
							LPWSTR    lpCmdLine,
							int       nCmdShow);

#endif // RoastEngineStd_h__

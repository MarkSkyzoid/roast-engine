#ifndef Memory_h__
#define Memory_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 14/08/2013																				  
// Memory macros and functions. Really important stuff used by all the engine subsystems to manage memory.
// Implementation ideas and references: http://molecularmusings.wordpress.com/2011/07/07/memory-system-part-2/
//=============================================================================================================

#include "MemoryAreas.h"
#include "MemoryArena.h"
#include "policies/MemoryPolicies.h"
#include "allocators/MemoryAllocators.h"

#include <memory>
#include <cstddef>
#include <type_traits>

#define DEFAULT_ALIGNMENT 16

//A little background: this macro calls a new with the so called placement syntax:
//the arena returns a valid memory address using a particular memory allocator. 
//The new operator will construct the object.
//Additional information (line number, file name) is used to keep track of allocation, for debugging purposes (ie. tracing memory leaks)
#define RE_NEW(type, arena)		new( arena.Allocate( sizeof(type), DEFAULT_ALIGNMENT, RE_SOURCEINFO ) ) type
#define RE_NEW_ALIGNED(type, arena, alignment) new( arena.Allocate( sizeof(type), alignment, RE_SOURCEINFO ) ) type

//Delete function:
#define RE_DELETE(object, arena)	Delete(object, arena)

//Uses template to get information about the object type.
template <typename T, class ARENA>
void Delete( T* object, ARENA& arena )
{
	if(object == nullptr)
		return; //Nothing to do here.

	object->~T(); //Destructor must be called explicitly as the object was constructed with placement new.

	arena.Free( (object) ); //Free the memory.
}


//POD (plain old data) optimization for the following routines.

template<typename T>
struct IsPOD
{
	static const bool value = std::is_pod<T>::value;
};

//Type based dispatching for NewArray overload (can't use true and false for overloading).
template <bool I>
struct IntToType
{
};

typedef IntToType<false> NonPODType;
typedef IntToType<true> PODType;

//Allocation for arrays.

//Partial template specialization structs to provide info to the following macro
template <class T>
struct TypeAndCount
{
};

template <class T, size_t N>
struct TypeAndCount<T[N]>
{
	typedef T type;
	static const size_t count = N;
};

//type will be in the format T[N] (ie. int[3]):
#define RE_ARRAY_NEW(type, arena)	NewArray<TypeAndCount<type>::type>( arena, TypeAndCount<type>::count, DEFAULT_ALIGNMENT, RE_SOURCEINFO, IntToType<IsPOD<TypeAndCount<type>::type>::value>() )
#define RE_ARRAY_NEW_ALIGNED(type, arena, alignment)	NewArray<TypeAndCount<type>::type>( arena, TypeAndCount<type>::count, alignment, RE_SOURCEINFO, IntToType<IsPOD<TypeAndCount<type>::type>::value>() )

//Helper function that allocates N instances of a certain type T.
template <typename T, class ARENA>
T* NewArray( ARENA& arena, size_t N, size_t alignment, const roast::debug::SourceInfo& source_info, NonPODType )
{
	//Anonymous union.
	//Can be used as variable. Every time a field is used, the memory in the union is treated as an instance of that field's type (avoids casting).
	union
	{
		void* as_void;
		size_t* as_size_t*;
		T* as_T;
	};

	//Note the + sizeof(size_t): this allocates a space for a "header" to keep track of the number of instances allocated (usually sizeof(size_t) = 4 bytes). 
	as_void = arena.Allocate( sizeof(T) * N + sizeof(size_t), alignment, source_info );

	//Store the number of instances in the firse sizeof(size_t) bytes and move the pointer to the first element of the array.
	*as_size_t++ = N;

	//Construct all the instances
	const T* const onePastLast = as_T + N; //Point to the "element" after the end of the array. Used as terminating point.
	while( as_T < onePastLast )
		new( as_T++ ) T;

	//Return the pointer to the first instance:
	return (as_T - N);
}

template <typename T, class ARENA>
T* NewArray( ARENA& arena, size_t N, size_t alignment, const roast::debug::SourceInfo& source_info, PODType )
{
	return static_cast( arena.Allocate( sizeof(T) * N, alignment, source_info ) ); //Don't need header with count this time as the destructors won't be called.
}

//Deallocation for arrays.

#define RE_ARRAY_DELETE(ptr, arena)		DeleteArray( ptr, arena )

//Utility function to deallocate arrays:
template <typename T, class ARENA>
void DeleteArray( T* ptr, ARENA& arena )
{
	DeleteArray( ptr, arena, IntToType<IsPOD<T>::value>() );
}

template <typename T, class ARENA>
void DeleteArray( T* ptr, ARENA& arena, NonPODType )
{
	union 
	{
		size_t* as_size_t;
		T* as_T;
	};

	as_T = ptr;

	//Goes back of sizeof(size_t) bytes to grab the "header" info (number of elements).
	const size_t N = as_size_t[-1];

	//Call destructors on elements in reverse order (follows C++ standard):
	for( size_t i = N; i > 0; --i )
		as_T[i - 1]->~T();

	//Free the memory.
	arena.Free(as_size_t - 1); //-1 because the array actually starts from the "header".
}

template <typename T, class ARENA>
void DeleteArray( T* ptr, ARENA& arena, PODType )
{
	arena.Free( ptr ); //Just deallocate memory for POD types.
}

#endif // Memory_h__

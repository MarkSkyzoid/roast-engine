#ifndef FrameAllocator_h__
#define FrameAllocator_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 10/08/2013																				  
// Class to allocate memory in A stack based fashion. Keeps frames (bookmarks) to deallocate memory. 
//=======================================================================================================

#include "RoastTypes.h"

//MACROs
#define ALIGNUP( Address, Bytes ) ( ( ((uint)Address) + (Bytes) - 1 ) & (~((Bytes) - 1)) )

namespace roast
{
	enum eFrameHeapType { HEAP_LOWER = 0, HEAP_UPPER };

	struct MemoryFrame
	{
		u8* framePointer;
		eFrameHeapType heap;
	};

	class FrameAllocator
	{
	private:
		int m_byteAlignment; //Alignment in bytes
		u8* m_memoryBlock;   //Block of memory allocated at init time.
		u8* m_baseAndCap[2]; //Base pointer = 0; Cap pointer = 1. These will remain constant.
		u8* m_frame[2];      //Lower frame pointer = 0; Upper frame pointer = 1. Will change with alloc/dealloc.

	public:
		//Construction
		FrameAllocator();
		~FrameAllocator();
		bool Init( size_t sizeInBytes, int byteAlignment );
		void Destroy();

		//Allocation/Deallocation
		void* Alloc( size_t bytes, eFrameHeapType heap ); //Allocate memory in the 
		void* Alloc( size_t bytes, int heap ); //For lazy people.

		void Dealloc( const MemoryFrame& frame ); //Release memory allocated up to the specified frame.

		//Get
		MemoryFrame GetFrame( eFrameHeapType heap ) const;
		MemoryFrame GetFrame( int heap ) const; //Again, lazy version.
	};
}


#endif // FrameAllocator_h__

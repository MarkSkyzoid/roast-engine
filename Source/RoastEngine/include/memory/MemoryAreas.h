#ifndef MemoryAreas_h__
#define MemoryAreas_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 30/08/2013																				  
// Various definitions for memory areas.
//=============================================================================================================

#include <memory>

namespace roast
{
	class HeapArea
	{
	private:
		size_t m_size;

		char* m_start;
		char* m_end;

	public:
		HeapArea( size_t size ) : m_size(size)
		{
			m_start = (char*)malloc( m_size * sizeof(char) );
			m_end = m_start + m_size;
		}


		~HeapArea()
		{
			free( m_start );
		}

		char* GetStart() const { return m_start; }
		char* GetEnd() const { return m_end; }
		size_t GetSize() const { return m_size; }
	};
}

#endif // MemoryAreas_h__

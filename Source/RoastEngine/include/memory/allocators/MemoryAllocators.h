#ifndef MemoryAllocators_h__
#define MemoryAllocators_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 30/08/2013																				  
// Common include for memory allocators
//=============================================================================================================

#include "LinearAllocator.h"
#include "PoolAllocator.h"

#endif // MemoryAllocators_h__

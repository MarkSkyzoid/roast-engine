#ifndef LinearAllocator_h__
#define LinearAllocator_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 10/08/2013																				  
// A simple linear allocator
//=======================================================================================================

namespace roast
{
	class LinearAllocator
	{
	private:
		char* m_start;
		char* m_end;
		char* m_current;

	public:
		explicit LinearAllocator( size_t size );
		LinearAllocator( void* start, void* end );

		void* Allocate( size_t size, size_t alignment, size_t offset );

		void Free( void* ptr );

		void Reset();

		size_t GetAllocationSize( void* ptr ) const;
	};
}

#endif // LinearAllocator_h__

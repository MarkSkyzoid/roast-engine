#ifndef PoolAllocator_h__
#define PoolAllocator_h__

//=======================================================================================================
// Author: Marco Vallario																			  	
// Date: 31/08/2013																				  
// A pool allocator (memory pool).
//=======================================================================================================

#include "RoastTypes.h"
#include "utils/DebugUtils.h"

namespace roast
{
	template <size_t maxChunkSize, size_t maxAlignment, uint numElements>
	class PoolAllocator
	{
	private:
		//Class to manage free slots in the pool.
		//TODO: Consider making this an Utility for the engine (it's lightweight, so can be used by other systems to).
		class FreeList
		{
		private:
			FreeList* m_next; //Pointer to the next free slot.

			//Keep the boundaries.
			char* m_start;
			char* m_end;

			size_t m_chunkSize;

		public:
			FreeList( void* start, void* end, size_t chunk_size, size_t max_alignment, uint num_elements )
			{	

				m_chunkSize = chunk_size;

				m_start = static_cast<char*>( start );
				m_start = static_cast<char*>( roast::pointerUtils::AlignUp(m_start, max_alignment) ); //Align the cursor.

				m_end = static_cast<char*> (end);

				Reset(); //Create the pool.
			}

			inline void* Obtain()
			{
				if( m_next == nullptr )
				{
					return nullptr;
				}

				FreeList* head = m_next;
				m_next = head->m_next;

				return head;
			}

			inline void Return( void* ptr )
			{
				if(ptr == nullptr)
					return; //Nothing to do here.

				//Return the pointer to the head of the free list.
				FreeList* head = static_cast<FreeList*>( ptr );
				head->m_next = m_next;
		
				m_next = head;
			}

			inline void Reset()
			{
				char* cursor = m_start;
				m_next = (FreeList*) cursor;

				cursor += m_chunkSize;

				//Initialize the free list.
				FreeList* new_slot = m_next;
				while( true )
				{
					if( (cursor + m_chunkSize ) > m_end)
					{
						break; //Out of memory, exit the loop. Might be worth adding some logging to this. TODO.
					}

					new_slot->m_next = (FreeList*) cursor;
					new_slot = (FreeList*) cursor; //Use what was the next element as the element of the next iteration.

					cursor += m_chunkSize;
				}

				new_slot->m_next = nullptr;
			}

		};

	private:
		char* m_start;
		char* m_end;
		char* m_current;

		size_t m_chunkSize;

		FreeList* m_freeListHead;

	public:
		explicit PoolAllocator( size_t size )
		{
			//TODO. 
			//NOTE: Not sure we need this. We might just want a default constructor that deduces the size out of the template params (makes much more sense), but I'll look into it later.
		}

		PoolAllocator( void* start, void* end ) 
		{
			m_start = m_current = (char*) start;
			m_end = (char*) end;

			m_chunkSize = (size_t) roast::pointerUtils::AlignUp( (size_t*) maxChunkSize, maxAlignment );

			//TODO: Decide where to store the first FreeList. Normal heap allocation might be a good guess.
			m_freeListHead = new FreeList(start, end, m_chunkSize, maxAlignment, numElements); 
		}

		void* Allocate( size_t size, size_t alignment, size_t offset )
		{
			return m_freeListHead->Obtain();
		}

		void Free( void* ptr )
		{
			m_freeListHead->Return(ptr);
		}

		void Reset()
		{
			m_freeListHead->Reset();
		}

		size_t GetAllocationSize( void* ptr ) const
		{
			return maxChunkSize;
		}
	};
}

#endif // PoolAllocator_h__

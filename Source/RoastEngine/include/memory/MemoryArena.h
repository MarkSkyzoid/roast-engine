#ifndef MemoryArena_h__
#define MemoryArena_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 26/08/2013																				  
// Memory Arena class designed using a policy based approach.
// Implementation ideas and references: http://molecularmusings.wordpress.com/2011/08/03/memory-system-part-5/
//=============================================================================================================

#include "utils/Utils.h"

namespace roast
{
	template <class AllocationPolicy, class ThreadPolicy, class BoundsCheckingPolicy, class MemoryTrackingPolicy, class MemoryTaggingPolicy>
	class MemoryArena
	{
	private:
		AllocationPolicy m_allocator;
		ThreadPolicy m_threadGuard;
		BoundsCheckingPolicy m_boundsChecker;
		MemoryTrackingPolicy m_memoryTracker;
		MemoryTaggingPolicy m_memoryTagger;

	public:
		template <class AreaPolicy>
		MemoryArena( const AreaPolicy& area ) : m_allocator( area.GetStart(), area.GetEnd() ) {}

		void* Allocate( size_t size, size_t alignment, const roast::debug::SourceInfo& source_info )
		{
			m_threadGuard.Enter();

			const size_t original_size = size;
			const size_t new_size = size + BoundsCheckingPolicy::SIZE_FRONT + BoundsCheckingPolicy::SIZE_BACK; //Adds some padding for later bounds checking.

			char* memory = static_cast<char*>( m_allocator.Allocate(new_size, alignment, BoundsCheckingPolicy::SIZE_FRONT) );

			m_boundsChecker.GuardFront( memory );
			m_memoryTagger.TagAllocation( memory + BoundsCheckingPolicy::SIZE_FRONT, original_size );
			m_boundsChecker.GuardBack( memory + BoundsCheckingPolicy::SIZE_FRONT + original_size );

			m_memoryTracker.OnAllocation( memory, new_size, alignment, source_info );

			m_threadGuard.Leave();

			return ( memory + BoundsCheckingPolicy::SIZE_FRONT ); //Just the useful memory.
		}

		void Free( void* ptr )
		{
			m_threadGuard.Enter();

			char* original_memory = static_cast<char*> ( ptr ) - BoundsCheckingPolicy::SIZE_FRONT;
			const size_t allocation_size = m_allocator.GetAllocationSize( original_memory ); 

			m_boundsChecker.CheckFront( original_memory );
			m_boundsChecker.CheckBack( original_memory );

			m_memoryTracker.OnDeallocation( original_memory );

			m_memoryTagger.TagDeallocation( original_memory, allocation_size );

			m_allocator.Free( original_memory );

			m_threadGuard.Leave();
		}

		AllocationPolicy* GetAllocator() { return &m_allocator; }
	};
}


#endif // MemoryArena_h__

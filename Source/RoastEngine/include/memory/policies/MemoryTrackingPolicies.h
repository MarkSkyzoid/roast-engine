#ifndef MemoryTrackingPolicies_h__
#define MemoryTrackingPolicies_h__

#include "utils/Utils.h"

#include <iostream>
#include <string>
#include <map>


//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 26/08/2013																				  
// Policies for tracking memory in memory management
//=============================================================================================================

namespace roast
{
	class NoMemoryTracking
	{
	public:
		inline void OnAllocation(void*, size_t, size_t, const roast::debug::SourceInfo&) const {}
		inline void OnDeallocation(void*) const {}
	};


	class SimpleMemoryTracking
	{
	private:
		mutable int m_allocations;

	public:
		SimpleMemoryTracking() : m_allocations(0) {}

		~SimpleMemoryTracking()
		{
			if( m_allocations )
				std::cout << "Some memory has leaked!" << std::endl; //TODO: This is crap. Use a logger.
		}

		inline void OnAllocation(void*, size_t, size_t, const roast::debug::SourceInfo&) const { ++m_allocations; }
		inline void OnDeallocation(void*) const { --m_allocations; }
	};

	class ExtendedMemoryTracking
	{
	private:

		struct AllocationInfo
		{
			roast::debug::SourceInfo sourceInfo;
			size_t size;
			size_t alignment;

			AllocationInfo(const roast::debug::SourceInfo& source_info, size_t n_size, size_t n_alignment) : sourceInfo(source_info), size(n_size), alignment(n_alignment) {}
		};

		mutable std::map<void*, AllocationInfo> m_allocations;

	public:
		ExtendedMemoryTracking() {}

		~ExtendedMemoryTracking()
		{
			if( m_allocations.empty() == false )
			{
				for ( auto& a : m_allocations )
				{
					std::cout << "Leaked " << a.second.size << "(" << a.second.alignment << ") bytes : " << a.second.sourceInfo.file << " : " << a.second.sourceInfo.line << std::endl;
				}
			}; //TODO: This is crap. Use a logger.
		}

		inline void OnAllocation(void* ptr, size_t size, size_t aligment, const roast::debug::SourceInfo& source_info) const 
		{
			m_allocations.insert( std::pair<void*, AllocationInfo>( ptr, AllocationInfo(source_info, size, aligment) ) );
		}
		inline void OnDeallocation(void* ptr) const 
		{ 
			m_allocations.erase( ptr ); 
		}
	};
}

#endif // MemoryTrackingPolicies_h__

#ifndef MemoryTaggingPolicies_h__
#define MemoryTaggingPolicies_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 26/08/2013																				  
// Policies for tagging memory in memory management
//=============================================================================================================

namespace roast
{
	class NoMemoryTagging
	{
	public:
		inline void TagAllocation(void*, size_t) const {}
		inline void TagDeallocation(void*, size_t) const {}
	};
}

#endif // MemoryTaggingPolicies_h__

#ifndef ThreadPolicies_h__
#define ThreadPolicies_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 26/08/2013																				  
// Policies for thread safety in memory management
//=============================================================================================================

namespace roast
{
	class SingleThreadPolicy
	{
	public:
		inline void Enter() {}
		inline void Leave() {}
	};
}

#endif // ThreadPolicies_h__

#ifndef MemoryPolicies_h__
#define MemoryPolicies_h__

//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 26/08/2013																				  
// Common include for memory policies
//=============================================================================================================

#include "ThreadPolicies.h"
#include "BoundsCheckingPolicies.h"
#include "MemoryTaggingPolicies.h"
#include "MemoryTrackingPolicies.h"

#endif // MemoryPolicies_h__

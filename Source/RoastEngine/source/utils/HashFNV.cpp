#include "utils/HashFNV.h"

unsigned int HashFNV(const char* s)
{
	unsigned int h = 2166136261u;
	const char* c = s;

	while(*c)
	{
		h = h ^ static_cast<unsigned int>(*c) * 16777619u;
		++c;
	}

	return h;
}
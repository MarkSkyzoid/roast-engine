#include "utils\windowshelper\RenderWindow.h"

//Hooks into the windows message system.
//Use the Custom Window Procedure if there is one, otherwise uses a default one.
LRESULT CALLBACK InternalWindowProc( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam )
{
	LONG_PTR ObjPtr = GetWindowLongPtr(hwnd, 0);

	if (ObjPtr == 0) {
		return( DefWindowProc( hwnd, msg, wparam, lparam ) );
	} else {
		return( ((roast::IWndProc*)ObjPtr)->WindowProc(hwnd, msg, wparam, lparam) );
	}
}

namespace roast
{
	RenderWindow::RenderWindow() :	m_hwnd(0),
									m_width( 640 ),
									m_height( 480 ),
									m_left( 100 ),
									m_top( 100 ),
									m_caption( L"" ),
									m_style( WS_OVERLAPPEDWINDOW | WS_VISIBLE ),
									m_swapChain( -1 )
	{

	}

	RenderWindow::~RenderWindow()
	{
		Shutdown();
	}

	void RenderWindow::Init( IWndProc* wind_proc_obj )
	{
		if(wind_proc_obj == nullptr)
			return; //TODO: CONSIDER RETURNING ERROR CODE.

		WNDCLASSEX wc;

		memset(&wc, 0, sizeof(wc));
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = InternalWindowProc;
		wc.cbClsExtra		= 0;
		wc.cbWndExtra		= sizeof(wind_proc_obj);
		wc.hInstance		= 0;
		wc.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
		wc.hCursor			= LoadCursor(NULL, IDC_ARROW); 
		wc.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
		wc.lpszMenuName		= NULL;
		wc.lpszClassName	= L"HieroglyphWin32";
		wc.hIconSm			= LoadIcon(NULL, IDI_APPLICATION);

		wind_proc_obj->BeforeRegisterWindowClass(wc);

		// Register the window class
		RegisterClassEx(&wc);

		// Specify the window style
		m_style = (WS_OVERLAPPEDWINDOW | WS_VISIBLE);


		// Record the desired device size
		RECT rc;
		rc.top = rc.left = 0;
		rc.right = m_width;
		rc.bottom = m_height;

		// Adjust the window size for correct device size
		AdjustWindowRect(&rc, m_style, FALSE);

		long lwidth = rc.right - rc.left;
		long lheight = rc.bottom - rc.top;

		long lleft = (long)m_left;	
		long ltop = (long)m_top;


		// Create an instance of the window
		m_hwnd = CreateWindowEx(
			NULL,							// extended style
			wc.lpszClassName, 				// class name
			m_caption.c_str(),				// instance title
			m_style,						// window style
			lleft, ltop,					// initial x, y
			lwidth,							// initial width
			lheight,						// initial height
			NULL,							// handle to parent 
			NULL,							// handle to menu
			NULL,							// instance of this application
			NULL );							// extra creation parms

		if (m_hwnd) 
		{
			// Set in the "extra" bytes the pointer to the IWindowProc object
			// which handles messages for the window
			SetWindowLongPtr(m_hwnd, 0, (LONG_PTR)wind_proc_obj);
			// Initially display the window
			ShowWindow( m_hwnd, SW_SHOWNORMAL );
			UpdateWindow( m_hwnd );
		}
	}

	void RenderWindow::Shutdown()
	{
		if ( m_hwnd )
			DestroyWindow( m_hwnd );

		m_hwnd = 0;
	}

	void RenderWindow::Draw()
	{

	}

	void RenderWindow::Resize( int width, int height )
	{
		//As this method is called by a resize event, we just need to update the local state (no need to call UpdateState).
		m_width = width;
		m_height = height;
	}

	void RenderWindow::SetWidth( int width )
	{
		m_width = width;

		UpdateState();
	}

	void RenderWindow::SetHeight( int height )
	{
		m_height = height;

		UpdateState();
	}

	void RenderWindow::SetSize( int width, int height )
	{
		m_width = width;
		m_height = height;

		UpdateState();
	}

	void RenderWindow::SetPosition( int left, int top )
	{
		m_left = left;
		m_top = top;

		UpdateState();
	}

	void RenderWindow::SetCaption( const std::wstring& caption )
	{
		m_caption;

		if(m_hwnd != 0)
		{
			SetWindowText(m_hwnd, m_caption.c_str());
		}
	}

	void RenderWindow::SetStyle( DWORD style )
	{
		m_style = style;

		SetWindowLongPtr(m_hwnd, GWL_EXSTYLE, m_style);
	}

	int RenderWindow::GetWidth() const
	{
		RECT rect;
		GetClientRect(m_hwnd, &rect);

		return (rect.right - rect.left);
	}

	int RenderWindow::GetHeight() const
	{
		RECT rect;
		GetClientRect(m_hwnd, &rect);

		return (rect.bottom - rect.top);
	}

	int RenderWindow::GetLeft() const
	{
		POINT point;
		point.x = 0;
		point.y = 0;

		ClientToScreen(m_hwnd, &point);

		return point.x;
	}

	int RenderWindow::GetTop() const
	{
		POINT point;
		point.x = 0;
		point.y = 0;

		ClientToScreen(m_hwnd, &point);

		return point.y;
	}

	void RenderWindow::UpdateState()
	{
		if(m_hwnd != 0)
		{
			RECT client_rect;
			client_rect.left = 0;
			client_rect.top = 0;
			client_rect.right = m_width;
			client_rect.bottom = m_height;

			RECT window_rect = client_rect;
			AdjustWindowRect(&window_rect, m_style, FALSE);

			int deltaX = ( window_rect.right - client_rect.right ) / 2;
			int deltaY = ( window_rect.bottom - client_rect.bottom ) / 2;

			MoveWindow( m_hwnd, m_left - deltaX, m_top - deltaY, 
				m_width + deltaX * 2, m_height + deltaY * 2, true );
		}
	}

	bool RenderWindow::Update()
	{
		MSG msg;

		// Initialize the message structure.
		ZeroMemory(&msg, sizeof(MSG));

		if( PeekMessage(&msg, NULL, 0, 0, PM_REMOVE ) )
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			// On quit return false.
			if(msg.message == WM_QUIT)
			{
				return false;
			}
		}

		return true;
	}

}
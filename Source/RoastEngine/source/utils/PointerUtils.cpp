#include "utils/PointerUtils.h"

namespace roast
{
	namespace pointerUtils
	{
		//Aligns a pointer to the next aligned address.
		void* AlignUp(void* const ptr, size_t alignment)
		{
			uint address = (uint) ptr;
			uint mask = alignment - 1;
			uint misalignment = address & mask;
			uint adjustment = alignment - misalignment;  
			return (void*)(address + adjustment * (misalignment != 0)); 
		}
	}
}
#include "math/Vector.h"

namespace roast
{
	namespace math
	{
		const Vec3 Vec3::GUpVec(0, 1, 0);
		const Vec3 Vec3::GRightVec(1, 0, 0);
		const Vec3 Vec3::GForwardVec(0, 0, 1);
	}
}
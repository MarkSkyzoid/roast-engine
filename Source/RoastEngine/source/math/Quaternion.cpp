#include "math/Quaternion.h"

namespace roast
{
	namespace math
	{
		 const Quaternion Quaternion::Identity(D3DXQUATERNION(0, 0, 0, 1));
	}
}
#include "math/Frustum.h"
#include "math/Math.h"

namespace roast
{
	namespace math
	{
		Frustum::Frustum()
		{
			m_fov	 = RE_PI_OVER_4; //Defaulted to 90 degrees.
			m_aspect = 1.0f;
			m_near	 = 1.0f; //1 meter away from eye.
			m_far	 = 1000.0f; //1000 meters away from eye.
		}

		void Frustum::Init( const float fov, const float aspect, const float near_dist, const float far_dist )
		{
			m_fov	 = fov;
			m_aspect = aspect;
			m_near	 = near_dist;
			m_far	 = far_dist;

			float tan_fov_over_2 = tan(m_fov * 0.5f);
			
			m_nearHeight = tan_fov_over_2 * m_near;
			m_nearWidth  = m_nearHeight * aspect;

			m_farHeight = tan_fov_over_2 * m_far;
			m_farWidth  = m_farHeight * aspect;
		}

		bool Frustum::Inside( const roast::math::Vec3& point )
		{
			for(int i = 0; i < eFrustumPlane_NumPlanes; ++i)
			{
				if(!m_planes[i].Inside(point))
				{
					return false;
				}
			}

			return true;
		}

		bool Frustum::Inside( const roast::math::Vec3& point, const float radius )
		{
			for(int i = 0; i < eFrustumPlane_NumPlanes; ++i)
			{
				if(!m_planes[i].Inside(point, radius))
				{
					return false;
				}
			}

			return true;
		}

		//TODO: Consider passing the view matrix to extract Camera Axis.
		void Frustum::SetPoint( const roast::math::Vec3& point, const roast::math::Vec3& forward, const roast::math::Vec3& up )
		{
			Vec3 right = up.Cross(forward);

			Vec3 near_center = point + forward * m_near;
			Vec3 far_center = point + forward * m_far;

			//Near plane vertices
			m_nearClip[0] = near_center + up * m_nearHeight - right * m_nearWidth; //Top Left 
			m_nearClip[1] = near_center + up * m_nearHeight + right * m_nearWidth; //Top Right
			m_nearClip[2] = near_center - up * m_nearHeight + right * m_nearWidth; //Bottom Right
			m_nearClip[3] = near_center - up * m_nearHeight - right * m_nearWidth; //Bottom Left

			//Far plane vertices
			m_farClip[0] = far_center + up * m_farHeight - right * m_farWidth; //Top Left 
			m_farClip[1] = far_center + up * m_farHeight + right * m_farWidth; //Top Right
			m_farClip[2] = far_center - up * m_farHeight + right * m_farWidth; //Bottom Right
			m_farClip[3] = far_center - up * m_farHeight - right * m_farWidth; //Bottom Left

			//Points are given in counter clockwise order to have the normal pointing in.

			m_planes[eFrustumPlane_Near].Init(m_nearClip[2], m_nearClip[1], m_nearClip[0]);
			m_planes[eFrustumPlane_Far].Init(m_farClip[0], m_farClip[1], m_farClip[2]);
			m_planes[eFrustumPlane_Left].Init(m_farClip[0], m_farClip[3], point);
			m_planes[eFrustumPlane_Right].Init(m_farClip[2], m_farClip[1], point);
			m_planes[eFrustumPlane_Top].Init(m_farClip[1], m_farClip[0], point);
			m_planes[eFrustumPlane_Bottom].Init(m_farClip[3], m_farClip[2], point);
		}

		void Frustum::Render()
		{
			//TODO: debug rendering.
		}
	}
}
#include "math/Matrix.h"
namespace roast
{
	namespace math
	{
		const Mat4x4 Mat4x4::Identity(D3DXMATRIX(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1));
	}
}
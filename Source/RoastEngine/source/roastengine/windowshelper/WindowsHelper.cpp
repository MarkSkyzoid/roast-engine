#include "utils/windowshelper/WindowsHelper.h"

#include "RoastTypes.h"

#include <shellapi.h>

#include <stdio.h>
#include <io.h>
#include <fcntl.h>

namespace roast
{
	namespace windowsHelper
	{
		static bool WindowCreated = false;
		static bool WindowCreatedCalled = false;

		static HWND WindowHWND = 0;

		HRESULT WINAPI RECreateWindow( UINT default_width /*= 640*/, UINT default_height /*= 480*/, const WCHAR* window_title /*= L"Roast Engine"*/, HINSTANCE hInstance /*= NULL*/, HICON hIcon /*= NULL*/, bool show_console /*= false */ )
		{
			WindowCreatedCalled = true;

			if(hInstance == NULL)
				hInstance = GetModuleHandle(NULL);
			
			WNDCLASSEX wc = { };
			WCHAR exe_path[MAX_PATH];
			GetModuleFileName(NULL, exe_path, MAX_PATH);

			if(hIcon == NULL)
				hIcon = ExtractIcon( hInstance, exe_path, 0 ); //

			const WCHAR* class_name = L"RoastEngine";

			wc.style = CS_DBLCLKS;
			wc.lpfnWndProc = WndProc;
			wc.cbClsExtra = 0;
			wc.cbWndExtra = 0;
			wc.hInstance = hInstance;
			wc.hIcon = hIcon;
			wc.hCursor = LoadCursor( NULL, IDC_ARROW );
			wc.hbrBackground = ( HBRUSH ) GetStockBrush ( BLACK_BRUSH );
			wc.lpszMenuName = NULL;
			wc.lpszClassName = class_name;
			wc.cbSize = sizeof(WNDCLASSEX);

			RegisterClassEx(&wc); //TODO: Check for errors on this.

			//Place at the center of the screen (it won't hide the console, if there is one).
			const uint x = (GetSystemMetrics(SM_CXSCREEN) - default_width)  / 2; 
			const uint y = (GetSystemMetrics(SM_CYSCREEN) - default_height) / 2;

			HWND hWnd = CreateWindowEx( WS_EX_APPWINDOW, class_name, window_title, WS_OVERLAPPEDWINDOW, x, y, default_width, default_height, NULL, NULL, hInstance, NULL );
			ShowWindow(hWnd, SW_SHOW);
			SetForegroundWindow(hWnd);
			SetFocus(hWnd);

			ShowCursor(true);

			if(show_console)
				ShowConsole();

			WindowCreated = true;
			WindowHWND = hWnd;

			return S_OK;
		}


		HRESULT WINAPI RECreateDevice( bool windowed, UINT screen_width, UINT screen_height )
		{
			HRESULT hresult = S_OK;

			if( !WindowCreated )
			{
				if( WindowCreatedCalled )
				{
					return E_FAIL;
				}

				hresult = RECreateWindow(screen_width, screen_height); //Try to create a new window.

				if( FAILED(hresult) )
				{
					return hresult;
				}
			}

			/* The following variables must be made accessible by the program(members or something like that) */
			IDXGISwapChain* swap_chain;
			ID3D11Device* device;
			ID3D11DeviceContext* device_context;

			DXGI_SWAP_CHAIN_DESC swap_chain_desc;
			ZeroMemory( &swap_chain_desc, sizeof(DXGI_SWAP_CHAIN_DESC) );

			swap_chain_desc.BufferCount = 1;
			swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swap_chain_desc.OutputWindow = WindowHWND;
			swap_chain_desc.SampleDesc.Count = 1;
			swap_chain_desc.Windowed = windowed;


			D3D11CreateDeviceAndSwapChain( NULL, 
										   D3D_DRIVER_TYPE_HARDWARE, 
										   NULL, 
										   NULL,
										   NULL,
										   NULL,
										   D3D11_SDK_VERSION,
										   &swap_chain_desc,
										   &swap_chain,
										   &device,
										   NULL,
										   &device_context);

			return hresult;
		}

		HRESULT WINAPI RECreateWindowAndDevice( bool windowed /*= true*/, UINT screen_width /*= 640*/, UINT screen_height /*= 480*/, const WCHAR* window_title /*= L"Roast Engine"*/, HINSTANCE hInstance /*= NULL*/, HICON hIcon /*= NULL*/, bool show_console /*= false */ )
		{
			HRESULT result = RECreateWindow(screen_width, screen_height, window_title, hInstance, hIcon, show_console);

			result &= RECreateDevice(windowed, screen_width, screen_height );

			return result;
		}

		//Temporary TODO: CREATE A PROPER ONE
		LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
		{
			switch(umessage)
			{
				// Check if the window is being destroyed.
			case WM_DESTROY:
				{
					PostQuitMessage(0);
					return 0;
				}

				// Check if the window is being closed.
			case WM_CLOSE:
				{
					PostQuitMessage(0);		
					return 0;
				}

				// All other messages pass to the message handler in the system class.
			default:
				{
					return DefWindowProc(hwnd, umessage, wparam, lparam);

				}
			}
		}

		void REWindowsMainLoop()
		{
			MSG msg;
			bool done, result;


			// Initialize the message structure.
			ZeroMemory(&msg, sizeof(MSG));

			// Loop until there is a quit message from the window or the user.
			done = false;
			while(!done)
			{
				// Handle the windows messages.
				if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}

				// If windows signals to end the application then exit out.
				if(msg.message == WM_QUIT)
				{
					done = true;
				}
				else
				{
					// Otherwise do the frame processing.
					result = true;//Frame();
					if(!result)
					{
						done = true;
					}
				}

			}

			return;

		}

		void REApplyDefaultDeviceSettings( REDeviceSettings* settings )
		{
			ZeroMemory( settings, sizeof( REDeviceSettings ) );

			settings->AdapterOrdinal = 0;
			settings->AutoCreateDepthStencil = true;
			settings->AutoDepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
			
#if defined(DEBUG) || defined(_DEBUG)
			settings->CreateFlags |= D3D10_CREATE_DEVICE_DEBUG;
#else
			settings->CreateFlags = 0;
#endif
			settings->DriverType = D3D_DRIVER_TYPE_HARDWARE;
			settings->Output = 0;
			settings->PresentFlags = 0;
			settings->sd.BufferCount = 2;
			settings->sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
			settings->sd.BufferDesc.Height = 480;
			settings->sd.BufferDesc.RefreshRate.Numerator = 60;
			settings->sd.BufferDesc.RefreshRate.Denominator = 1;
			settings->sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			settings->sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			settings->sd.BufferDesc.Width = 640;
			settings->sd.BufferUsage = 32;
			settings->sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
			settings->sd.OutputWindow =  GetHWnd();
			settings->sd.SampleDesc.Count = 1;
			settings->sd.SampleDesc.Quality = 0;
			settings->sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			settings->sd.Windowed = 1;
			settings->SyncInterval = 0;
		}

		HWND GetHWnd()
		{
			return WindowHWND;
		}

		void ShowConsole()
		{
			AllocConsole();

			HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
			int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
			FILE* hf_out = _fdopen(hCrt, "w");
			setvbuf(hf_out, NULL, _IONBF, 1);
			*stdout = *hf_out;

			HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
			hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
			FILE* hf_in = _fdopen(hCrt, "r");
			setvbuf(hf_in, NULL, _IONBF, 128);
			*stdin = *hf_in;
		}

		void HideConsole()
		{
			FreeConsole();
		}	
	}
}
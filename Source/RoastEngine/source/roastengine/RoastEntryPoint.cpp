//=============================================================================================================
// Author: Marco Vallario																			  	
// Date: 11/09/2013																				  
// Main Windows entry point!
//=============================================================================================================

#include "roastengine/RoastEngine.h"
namespace roast
{
	class DefaulWndProc : public IWndProc
	{
		virtual LRESULT WindowProc( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) 
		{
			switch(msg)
			{
				// Check if the window is being destroyed.
			case WM_DESTROY:
				{
					PostQuitMessage(0);
					return 0;
				}

				// Check if the window is being closed.
			case WM_CLOSE:
				{
					PostQuitMessage(0);		
					return 0;
				}

				// All other messages pass to the message handler in the system class.
			default:
				{
					return DefWindowProc(hwnd, msg, wparam, lparam);
				}
			}
		}

		virtual void BeforeRegisterWindowClass( WNDCLASSEX &wc ) 
		{
			//do nothing
		}

	};
}

INT WINAPI RoastMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	//#ifdef _DEBUG
	//	roast::windowsHelper::RECreateWindowAndDevice(true, 800, 600, L"Roast Engine", hInstance, NULL, true);
	//#else
	//	roast::windowsHelper::RECreateWindowAndDevice(true, 800, 600, L"Roast Engine", hInstance);
	//#endif
	//
	//	roast::windowsHelper::REWindowsMainLoop();

	roast::RenderWindow win;
	roast::DefaulWndProc wnd_proc;
	win.Init(&wnd_proc);
	
	while(win.Update())
	{
		// Do stuff.
	}

	return 0;
}

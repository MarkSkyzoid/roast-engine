#include "RoastTypes.h"

#include "memory/allocators/LinearAllocator.h"
#include "utils/Utils.h"

#include <memory>

namespace roast
{

	LinearAllocator::LinearAllocator( size_t size )
	{
		//TODO: Implement page allocator.
		//No area was specified so create one on the heap
		m_start = (char*) malloc( size * sizeof(char) );
		m_end = m_start + size;
		m_current = m_start;
	}

	LinearAllocator::LinearAllocator( void* start, void* end )
	{
		m_start = m_current = (char*) start;
		m_end = (char*) end;
	}

	void* LinearAllocator::Allocate( size_t size, size_t alignment, size_t offset )
	{
		const size_t size_offset = sizeof(size_t); //Keep few bytes to store allocation size.
		//4 byte allocation size | offset byte front guard | user memory | offset byte back guard

		char* ptr = static_cast<char*> ( pointerUtils::AlignUp(m_current + size_offset + offset, alignment) ); // This is the memory I give to the user
		m_current = ptr + size;

		if(m_current >= m_end)
		{
			//out of memory.
			return NULL;
		}

		//TODO: CHECK IF THIS WORK. NOT SURE. I WAS TIRED.
		union 
		{
			char* size_keeper_as_char;
			size_t* size_keeper_as_size_t;
		};

		size_keeper_as_char = ptr - offset - size_offset;
		*size_keeper_as_size_t = size;

		return ptr - offset; // This is the memory I give to the arena.
	}

	void LinearAllocator::Free( void* ptr )
	{
		//Do nothing.
	}

	void LinearAllocator::Reset()
	{
		//Bring current pointer to start.
		m_current = m_start;
	}

	size_t LinearAllocator::GetAllocationSize( void* ptr ) const
	{
		union
		{
			void* as_void;
			char* as_char;
			size_t* as_size_t;
		};

		// grab the allocation's size from the first N bytes before the user data
		as_void = ptr;
		as_char -= sizeof(size_t);
		return (*as_size_t);
	}
}
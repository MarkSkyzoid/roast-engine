#include "memory/FrameAllocator.h"
#include <memory>

namespace roast
{
	FrameAllocator::FrameAllocator() : m_byteAlignment(0), m_memoryBlock(0)
	{
		m_baseAndCap[0] = 0;
		m_baseAndCap[1] = 0;

		m_frame[0] = 0;
		m_frame[1] = 0;
		
	}

	FrameAllocator::~FrameAllocator()
	{
		Destroy();
	}

	//Initialize the Frame Allocator.
	//Must be called once.
	//Byte Alignment must be power of 2.
	bool FrameAllocator::Init( size_t sizeInBytes, int byteAlignment )
	{
		//Make sure sizeInBytes is multiple of byteAlignment:
		sizeInBytes = ALIGNUP( sizeInBytes, byteAlignment ); 

		//Allocate memory block:
		m_memoryBlock = (u8*)malloc( sizeInBytes + byteAlignment );

		if( m_memoryBlock == NULL )
		{
			//Out of memory. Error:
			return false;
		}

		m_byteAlignment = byteAlignment;

		//Base and Cap pointers:
		m_baseAndCap[0] = (u8*)ALIGNUP( m_memoryBlock, byteAlignment ); //base.
		m_baseAndCap[1] = (u8*)ALIGNUP( m_memoryBlock + sizeInBytes, byteAlignment ); //cap.

		//Now, lower and upper frame pointers:
		m_frame[0] = m_baseAndCap[0]; //Lower.
		m_frame[1] = m_baseAndCap[1]; //Upper.

		//Everything went well :)
		return true;
	}

	//Deallocate everything.
	void FrameAllocator::Destroy()
	{
		//Just free the pre-allocated memory block:
		free( m_memoryBlock ); //See ya!
	}

	//Tries to allocate memory from the memory block. 
	//A NULL pointer is returned if there is no more memory.
	void* FrameAllocator::Alloc( size_t bytes, eFrameHeapType heap )
	{
		//Align the requested size:
		bytes = ALIGNUP( bytes, m_byteAlignment );

		//Check if there is still memory:
		if( m_frame[0] + bytes > m_frame[1] )
		{
			//No more memory :(
			return NULL;
		}

		//Allocate memory:
		u8* memory = 0; //Final pointer.

		switch(heap)
		{
		case HEAP_UPPER:
			{
				m_frame[1] -= bytes; //Bump it down!
				memory = m_frame[1];
			}
			break;

		case HEAP_LOWER:
			{
				m_frame[0] += bytes; //Pull it up!
				memory = m_frame[0];
			}
			break;

		default:
			return NULL; //Invalid heap location.
		}

		return (void*)memory;
	}

	void* FrameAllocator::Alloc( size_t bytes, int heap )
	{
		return Alloc( bytes, static_cast<eFrameHeapType>(heap) );
	}

	//Deallocate to a particoular frame.
	void FrameAllocator::Dealloc( const MemoryFrame& frame )
	{
		//Just move the interested frame pointer to the one defined in the frame.
		m_frame[frame.heap] = frame.framePointer;
	}

	//Returns a frame which acts like a bookmark.
	//Can be later used to release memory allocated henceforth.
	MemoryFrame FrameAllocator::GetFrame( eFrameHeapType heap ) const
	{
		MemoryFrame frame;

		frame.framePointer = m_frame[heap];
		frame.heap = heap;

		return frame;
	}

	MemoryFrame FrameAllocator::GetFrame( int heap ) const
	{
		return GetFrame( static_cast<eFrameHeapType>(heap) );
	}
}